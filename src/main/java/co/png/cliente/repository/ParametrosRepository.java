package co.png.cliente.repository;

import co.png.cliente.model.Parametros;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ParametrosRepository extends JpaRepository<Parametros, Integer> {

    List<Parametros> findAll();
    List<Parametros> findByCodigo(String codigo);

    @Query("select p from Parametros p where p.idGrupo.id = :idGrupo")
    List<Parametros> findByGrupo(@Param("idGrupo") final Integer idGrupo);

    @Query("select p from Parametros p where p.idGrupo.id = :idGrupo and p.estado = true")
    List<Parametros> findByGrupoActivos(@Param("idGrupo") final Integer idGrupo);

    @Query("select p from Parametros p where p.idGrupo.id = :idGrupo AND p.codigo = :codigo")
    Parametros findByGrupoAndCodigo(@Param("idGrupo") final Integer idGrupo, @Param("codigo") final String codigo);

    @Query(value="SELECT (SELECT count(*) as equipos   FROM [equipos]) AS equipos, (SELECT count(*) as activos  FROM [equipos] where estado = 1) as activos, " +
    " ( SELECT COUNT(*)   FROM [equipos_inventario]) as inventario, (SELECT COUNT(*)   FROM [parametros] WHERE fk_grupo= 43) as sedes, " +
    " (SELECT  COUNT(*)   FROM [IPS_PRUEBA].[dbo].[calendario]) as programados, (SELECT  COUNT(*)   FROM [IPS_PRUEBA].[dbo].[calendario] WHERE estado = 1) as realizados", nativeQuery = true)
    Map<String, ?> getEstadisticas();





}
