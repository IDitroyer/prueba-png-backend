package co.png.cliente.repository;

import co.png.cliente.model.Grupos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface GruposRepository extends JpaRepository<Grupos, Integer> {

    List<Grupos> findAll();



    @Query("select g.id as id, g.nombre as nombre, g.descripcion as descripcion, (select count(p) from Parametros p where p.idGrupo.id = g.id) as numeroParametros from Grupos g")
    List<Map<String, Object>> findAllWithNoParametros();

}
