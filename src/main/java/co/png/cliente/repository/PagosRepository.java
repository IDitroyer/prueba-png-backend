package co.png.cliente.repository;

import co.png.cliente.model.Pagos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface PagosRepository extends JpaRepository<Pagos, Integer> {

    List<Pagos> findAll();

    @Query("select p from Pagos p where p.idUsuario.id = :id")
    List<Pagos> findByUsuario(@Param("id") int id);

}
