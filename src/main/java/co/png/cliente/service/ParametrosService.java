package co.png.cliente.service;

import co.png.cliente.model.Parametros;
import co.png.cliente.repository.ParametrosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ParametrosService {

    @Autowired
    ParametrosRepository parametrosRepository;


    public List<Parametros> findAll() {
        return parametrosRepository.findAll();
    }

    public List<Parametros> findByGrupo(Integer idGrupo) {
        return parametrosRepository.findByGrupo(idGrupo);
    }

    public List<Parametros> findByGrupoActivos(Integer idGrupo) {
        return parametrosRepository.findByGrupoActivos(idGrupo);
    }

    public Parametros findByGrupoAndCodigo(Integer idGrupo, String codigo) {
        return parametrosRepository.findByGrupoAndCodigo(idGrupo, codigo);
    }

    public Parametros save(Parametros parametros) {
        return parametrosRepository.save(parametros);
    }

    public void deleteById(Integer id) {
        parametrosRepository.deleteById(id);
    }


    public Map<String, ?> getEstadisticas() {
        return parametrosRepository.getEstadisticas();
    }





}
