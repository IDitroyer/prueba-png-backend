package co.png.cliente.service;

import co.png.cliente.model.Pagos;
import co.png.cliente.repository.PagosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


@Service
public class PagosService {

    @Autowired
    PagosRepository pagosRepository;


    public List<Pagos> findAll() {
        return pagosRepository.findAll();
    }

    public Optional<Pagos> findById(int id) {return pagosRepository.findById(id);}


    public Pagos save(Pagos pagos) {
        return pagosRepository.save(pagos);
    }


    public void deleteById(Integer id) {
        pagosRepository.deleteById(id);
    }

    public List<Pagos> findByUsuario(Integer id) {return pagosRepository.findByUsuario(id);}

    public String subirSoporte(MultipartFile archivo) throws IOException {
        String nombreArchivo = UUID.randomUUID().toString() + "_" + archivo.getOriginalFilename().replace(" ", "");
        Path rutaArchivo = Paths.get("C:/png/uploads").resolve(nombreArchivo).toAbsolutePath();
        Files.copy(archivo.getInputStream(), rutaArchivo);
        return nombreArchivo;
    }



}
