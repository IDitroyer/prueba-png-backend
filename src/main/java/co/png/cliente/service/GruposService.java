package co.png.cliente.service;


import co.png.cliente.model.Grupos;
import co.png.cliente.repository.GruposRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class GruposService {

    @Autowired
    GruposRepository gruposRepository;


    public List<Grupos> findAll() {
        return gruposRepository.findAll();
    }


    public List<Map<String, Object>> findAllWithNoParametros() {
        return gruposRepository.findAllWithNoParametros();
    }


    public Grupos save(Grupos grupos) {
        return gruposRepository.save(grupos);
    }


    public void deleteById(Integer id) {
        gruposRepository.deleteById(id);
    }


}
