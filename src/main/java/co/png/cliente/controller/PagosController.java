package co.png.cliente.controller;

import co.png.cliente.model.Pagos;
import co.png.cliente.security.CurrentUser;
import co.png.cliente.security.UserPrincipal;
import co.png.cliente.service.PagosService;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@CrossOrigin(origins = {"*"})
@RequestMapping("/api/v1")
@RestController
public class PagosController {

    @Autowired
    private PagosService service;

    @GetMapping("/pagos/usuario/{id}")
    public List<Pagos> findByUsuario(@PathVariable("id") int id) {
        return service.findByUsuario(id);
    }


    @PostMapping("/pagos/save")
    public ResponseEntity<?> guardarPago(@RequestBody Pagos pago
    ) throws ParseException, java.text.ParseException {
        Map<String, Object> response = new HashMap();

        service.save(pago);
        response.put("message", "Se ha guardado el pago correctamente");
        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @GetMapping("/pagos/usuario")
    public List<Pagos> getByUser(@CurrentUser UserPrincipal principal) {
        return service.findByUsuario(principal.getId());


    }


    @PostMapping("/pagos/subirsoporte")
    public ResponseEntity<?> subirSoporte(
            @RequestParam(required = true) MultipartFile soporte,
            @RequestParam(required = true) int idPago) throws IOException {
        Map<String, Object> response = new HashMap<>();

        Optional<Pagos> p = service.findById(idPago);
        Pagos pago = p.get();
        pago.setSoporte(service.subirSoporte(soporte));
        service.save(pago);

        response.put("message", "El soporte se ha subido correctamente");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PostMapping("/pagos/crear")
    public ResponseEntity<?> crearPago(@RequestBody Pagos pago
    ) throws ParseException, java.text.ParseException {
        Map<String, Object> response = new HashMap();
        Pagos body = pago;
        body.setActivo(true);
        service.save(body);
        response.put("message", "Se ha guardado el pago correctamente");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("pagos/eliminar/{id}")
    public ResponseEntity<?> eliminarPago(@PathVariable("id") int idPago) {
        Map<String, Object> response = new HashMap();
        service.deleteById(idPago);
        response.put("message", "Se ha eliminado el pago correctamente");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("pagos/soporte/{nombreArchivo:.+}")
    public ResponseEntity<Resource> verSoporte(@PathVariable("nombreArchivo") String nombreArchivo) {
        Path rutaArchivo = Paths.get("C:/png/uploads").resolve(nombreArchivo).toAbsolutePath();
        Resource recurso = null;
        try {
            recurso = new UrlResource(rutaArchivo.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (!recurso.exists() && !recurso.isReadable()) {
            throw new RuntimeException("Error no se pudo cargar : " + nombreArchivo);
        }
        HttpHeaders cabecera = new HttpHeaders();
        cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");
        return new ResponseEntity<>(recurso, cabecera, HttpStatus.OK);
    }





}
