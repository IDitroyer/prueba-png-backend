package co.png.cliente.controller;

import co.png.cliente.model.Grupos;
import co.png.cliente.service.GruposService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"*"})
@RequestMapping("/api/v1")
@RestController
public class GruposController {

    @Autowired
    private GruposService service;

    @GetMapping("/grupos/list")
    public List<Grupos> findAll() {
        return service.findAll();
    }

    @GetMapping("/grupos/listwp")
    public List<Map<String, Object>> findAllWithNoParametros() {
        return service.findAllWithNoParametros();
    }

    @PostMapping("/grupos/save")
    public ResponseEntity<?> save(@RequestBody Grupos grupos) {

        Map<String, Object> response = new HashMap<>();
        
   
        try {
            response.put("grupo", service.save(grupos));
        } catch (DataAccessException e) {
            response.put("message", "fallo al guardar el grupo");
            response.put("error", e.getMessage()+": "+e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


    @DeleteMapping("/grupos/delete/{idGrupo}")
    public ResponseEntity<?> deleteParametros(@PathVariable("idGrupo") final Integer idGrupo) {

        Map<String, Object> response = new HashMap<>();
    
        try {
            service.deleteById(idGrupo);
            response.put("mesaage", "El grupo ha sido eliminado");
        } catch (DataAccessException e) {
            response.put("message", "fallo al eliminar el grupo");
            response.put("error", e.getMessage()+": "+e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }



}
