package co.png.cliente.controller;


import co.png.cliente.model.Parametros;
import co.png.cliente.service.ParametrosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"*"})
@RequestMapping("/api/v1")
@RestController
public class ParametrosController {

    @Autowired
    private ParametrosService service;

    @GetMapping("/parametros/list")
    public List<Parametros> findAll() {
        return service.findAll();
    }

    @GetMapping("/parametros/grupo/{idGrupo}")
    public List<Parametros> findByGrupo(@PathVariable("idGrupo") final Integer idGrupo) {
        return service.findByGrupo(idGrupo);
    }

    @GetMapping("/parametros/grupo/activos/{idGrupo}")
    public List<Parametros> findByGrupoActivos(@PathVariable("idGrupo") final Integer idGrupo) {
        return service.findByGrupoActivos(idGrupo);
    }

    @GetMapping("/parametros/grupo/codigo/{idGrupo}/{codigo}")
    public Parametros findByGrupoAndCodigo(@PathVariable("idGrupo") final Integer idGrupo, @PathVariable("codigo") final String codigo) {
        return service.findByGrupoAndCodigo(idGrupo, codigo);
    }





    @GetMapping("/estadisticas")
    public Map<String, ?> getEstadisticas() {
        return service.getEstadisticas();
    }



    @PostMapping("/parametros/save")
    public ResponseEntity<?> saveParametros(@RequestBody Parametros parametros) {
        Map<String, Object> response = new HashMap<>();
        Parametros pa = parametros;
        if(pa.getEstado() == null) {
            pa.setEstado(true);
        }
        try {
            response.put("parametro", service.save(pa));
        } catch (DataAccessException e) {
            response.put("message", "fallo al guardar el parametro");
            response.put("error", e.getMessage()+": "+e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


    @DeleteMapping("/parametros/delete/{idParametro}")
    public ResponseEntity<?> deleteParametros(@PathVariable("idParametro") final Integer idParametro) {
        Map<String, Object> response = new HashMap<>();
        try {
            service.deleteById(idParametro);
            response.put("mesaage", "El parametro ha sido eliminado");
        } catch (DataAccessException e) {
            response.put("message", "fallo al eliminar el parametro");
            response.put("error", e.getMessage()+": "+e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/parametros/consecutivos/orden")
    public  ResponseEntity<?>  getConsecutivosOrdneyCotizacion() {
        Map<String, Object> response = new HashMap<>();
        try {

            Parametros p_orden = service.findByGrupoAndCodigo( 0, "9996");
            p_orden.setValor( BigDecimal.valueOf( p_orden.getValor().intValue() + 1) );

            Parametros p_cotizacion = service.findByGrupoAndCodigo( 0, "9997");
            p_cotizacion.setValor( BigDecimal.valueOf( p_cotizacion.getValor().intValue() + 1) );

            response.put("p_orden", service.save(p_orden));
            response.put("p_cotizacion", service.save(p_cotizacion));

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (DataAccessException e) {
            response.put("message", "error al consultar");
            response.put("error", e.getMessage()+": "+e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
