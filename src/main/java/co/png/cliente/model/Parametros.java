package co.png.cliente.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Table(name = "parametros")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
public class Parametros implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private int id;
    @ManyToOne
    @JoinColumn(name = "id_grupo", referencedColumnName = "id", nullable = false)
    private Grupos idGrupo;
    @Basic
    @Column(name = "codigo")
    private String codigo;
    @Basic
    @Column(name = "valor")
    private BigDecimal valor;
    @Basic
    @Column(name = "fecha")
    private Timestamp fecha;
    @Basic
    @Column(name = "nombre")
    private String nombre;
    @Basic
    @Column(name = "nombre_corto")
    private String nombreCorto;
    @Basic
    @Column(name = "estado")
    private Boolean estado;

}
