package co.png.cliente.model;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "pagos")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
public class Pagos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
    private Usuarios idUsuario;

    @Basic
    @Column(name = "fecha")
    private Date fecha;

    //medios de pago = idParametro  : 1
    @ManyToOne
    @JoinColumn(name = "tipo", referencedColumnName = "id", nullable = false)
    private Parametros tipo;


    @Basic
    @Column(name = "soporte")
    private String soporte;

    @Basic
    @Column(name = "activo")
    private Boolean activo;

    @Basic
    @Column(name = "referencia")
    private String referencia;

}