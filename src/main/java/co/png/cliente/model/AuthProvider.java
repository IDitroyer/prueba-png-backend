package co.png.cliente.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
